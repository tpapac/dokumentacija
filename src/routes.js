import home from './components/home';
import Sustav from './components/stranice/SUMoodle/SUMoodle/Sustav';
import eucenje from './components/stranice/Eucenje';
import prijava from './components/stranice/SUMoodle/SUMoodle/prijava';
import sucelje from './components/stranice/SUMoodle/SUMoodle/sucelje';
import osobniPodaci from './components/stranice/SUMoodle/SUMoodle/osobniPodaci';
import Ekolegij from './components/stranice/SUMoodle/E-kolegij/Ekolegij';
import Organizacija from './components/stranice/SUMoodle/E-kolegij/Organizacija';
import kreiranjekolegija from './components/stranice/SUMoodle/E-kolegij/kreiranjekolegija';
import administracija from './components/stranice/SUMoodle/E-kolegij/administracija';
import objavanovosti from './components/stranice/SUMoodle/E-kolegij/objavanovosti';
import Korisnici from './components/stranice/SUMoodle/Korisnici/Korisnici';
import uloge from './components/stranice/SUMoodle/Korisnici/uloge';
import upis from './components/stranice/SUMoodle/Korisnici/upis';
import grupe from './components/stranice/SUMoodle/Korisnici/grupe';
import korisnicionline from './components/stranice/SUMoodle/Korisnici/korisnicionline';
import zajednicke from './components/stranice/SUMoodle/Zajednicke/zajednicke';
import uredjivanje from './components/stranice/SUMoodle/Zajednicke/uredjivanje';
import AlatiZaUredjivanje from './components/stranice/SUMoodle/Zajednicke/AlatiZaUredjivanje';
import AdministracijaDatoteka from './components/stranice/SUMoodle/Zajednicke/AdministracijaDatoteka';
import DragDrop from './components/stranice/SUMoodle/Zajednicke/DragDrop';
import Ogranicenjedostupnosti from './components/stranice/SUMoodle/Zajednicke/Ogranicenjedostupnosti';


export const routes = [
    {
        path: '/', component: home,
    },
    {
        path: '/SUMoodle/SUMoodle/Sustav', component: Sustav,
    },
    {
        path: '/SUMoodle', component: eucenje,
    },
    {
        path: '/SUMoodle/SUMoodle/prijava', component: prijava,
    },
    {
        path: '/SUMoodle/SUMoodle/sucelje', component: sucelje,
    },
    {
        path: '/SUMoodle/SUMoodle/osobniPodaci', component: osobniPodaci,
    },
    {
        path: '/SUMoodle/E-kolegij', component: Ekolegij,
    },
    {
        path: '/SUMoodle/E-kolegij/Organizacija', component: Organizacija,
    },
    {
        path: '/SUMoodle/E-kolegij/kreiranjekolegija', component: kreiranjekolegija,
    },
    {
        path: '/SUMoodle/E-kolegij/administracija', component: administracija,
    },
    {
        path: '/SUMoodle/E-kolegij/objavanovosti', component: objavanovosti,
    },
    {
        path: '/SUMoodle/korisnici', component: Korisnici,
    },
    {
        path: '/SUMoodle/korisnici/uloge', component: uloge,
    },
    {
        path: '/SUMoodle/korisnici/upis', component: upis,
    },
    {
        path: '/SUMoodle/korisnici/grupe', component: grupe,
    },
    {
        path: '/SUMoodle/korisnici/korisnicionline', component: korisnicionline,
    },
    {
        path: '/SUMoodle/Zajednicke/zajednicke', component: zajednicke,
    },
    {
        path: '/SUMoodle/Zajednicke/uredjivanje', component: uredjivanje,
    },
    {
        path: '/SUMoodle/Zajednicke/AlatiZaUredjivanje', component: AlatiZaUredjivanje,
    },
    {
        path: '/SUMoodle/Zajednicke/AdministracijaDatoteka', component: AdministracijaDatoteka,
    },
    {
        path: '/SUMoodle/Zajednicke/Drag&Drop', component: DragDrop,
    },
    {
        path: '/SUMoodle/Zajednicke/Ogranicenjedostupnosti', component: Ogranicenjedostupnosti,
    },
]

